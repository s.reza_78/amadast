<?php

namespace App\Providers;

use App\DB\CustomerRepo\CustomerRepo;
use App\DB\interfaces\CustomerRepoInterface;
use App\DB\interfaces\OrderRepoInterface;
use App\DB\interfaces\ShopRepoInterface;
use App\DB\OrderRepo\OrderRepo;
use App\DB\shopRepo\ShopRepo;
use Illuminate\Support\ServiceProvider;

class RepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        app()->bind(CustomerRepoInterface::class , CustomerRepo::class);

        app()->bind(OrderRepoInterface::class , OrderRepo::class);

        app()->bind(ShopRepoInterface::class , ShopRepo::class);


    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
