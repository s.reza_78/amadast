<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'seller_id', 'description'];


    public function seller()
    {

        return $this->belongsTo(Seller::class);
    }




    public function orders()
    {

        return $this->hasMany(Order::class);
    }
}
