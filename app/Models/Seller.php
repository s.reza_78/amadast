<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    use HasFactory;


    protected $fillable = [
        'national_code',
        'address',
        'ostan',
        'city',
        'shaba',
        'account_name'
    ];
    public function users()
    {
        return $this->morphToMany(User::class, 'userable');
    }


    public function customers()
    {
        return $this->hasMany(Customer::class);
    }
    public function shops()
    {
        return $this->hasMany(Shop::class);
    }
}
