<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class userable extends Model
{
    use HasFactory;

    protected $fillable = ['userable_type'];


    public function users(){
        return $this->belongsTo(User::class);
    }
}
