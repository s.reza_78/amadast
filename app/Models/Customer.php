<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;


    protected $fillable = ['seller_id'];

    protected $with = ['user'];
    public function user()
    {
        return $this->morphToMany(User::class, 'userable');
    }

    public function seller(){

        return $this->belongsTo(Seller::class);
    }
}
