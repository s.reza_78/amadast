<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'shop_id',
        'support_code',
        'description',
        'origin_address',
        'destination_address',
        'weight',
        'recive_status',
        'invoice',
        'payment_status',
        'origin_city',
        'destination_city',
        'sendingـmethod'
    ];



    public function shop(){
        return $this->belongsTo(Shop::class);
    }
}
