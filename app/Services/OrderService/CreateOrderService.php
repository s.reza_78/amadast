<?php

namespace App\Services\OrderService;

use App\Models\Order;
use App\Services\Logestic\Tipax;

class CreateOrderService
{


    public function create($request)
    {

        $order['shop_id'] = $request->shop_id;
        $order['support_code'] = uniqid();
        $order['origin_address'] = $request->origin_address;
        $order['destination_address'] = $request->destination_address;
        $order['weight'] = $request->weight;
        $order['origin_city'] = $request->origin_city;
        $order['destination_city'] = $request->destination_city;
        $order['sendingـmethod'] = $request->sendingـmethod;

        $order['invoice'] = $this->logestic($request);

        if(isset($request->description)){
            $order['description']= $request->description;

        }

        $order = Order::create($order);

        return $order;
    }





    public function logestic($request)
    {
        switch ($request->sendingـmethod) {

            case 'tipax':
                $tipax = new Tipax;
                return $tipax->create($request);
                // case 'post' : $post = resolve(Post::class);
                //               $post->create($request);
        }
    }
}
