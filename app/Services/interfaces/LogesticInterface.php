<?php

namespace App\Services\Interfaces;

interface LogesticInterface
{


    public function calculatePrice($weight, $origin_city, $destination_city);

    public function create($request);
}
