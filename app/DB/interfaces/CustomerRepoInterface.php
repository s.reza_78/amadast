<?php

namespace App\DB\interfaces;

interface CustomerRepoInterface{



    public function create($request);
    public function getOneCustomerById($id);
    public function getAllSellerCustomers();
    public function update($request,$id);
    public function delete($id);



}
