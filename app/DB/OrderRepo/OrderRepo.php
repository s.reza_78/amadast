<?php

namespace App\DB\OrderRepo;

use App\Models\Order;
use App\DB\interfaces\OrderRepoInterface;


class OrderRepo implements OrderRepoInterface
{



    public function create($data)
    {

        return Order::create($data);
    }

    public function getAllOrders()
    {
        return Order::all();
    }
}
