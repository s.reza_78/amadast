<?php

namespace App\DB\CustomerRepo;

use App\DB\interfaces\CustomerRepoInterface;
use App\DB\UserRepo\UserRepo;
use App\Models\Customer;
use App\Models\Seller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CustomerRepo implements CustomerRepoInterface
{


    public function create($data)
    {
        $seller = Auth::user();
        DB::beginTransaction();

        try {
            $userRepo = new UserRepo;
            $user = $userRepo->create($data);

            $customer = Customer::create(['seller_id' => $seller->id]);
            $customer->user()->save($user);

            DB::commit();
            return ['customer' => $customer, 'user' => $user];
        } catch (\Exception $e) {
            DB::rollBack();
            return ["error" => $e->getMessage()];
        }
    }


    public function getAllSellerCustomers()
    {
        return Customer::where('seller_id', Auth::user()->userable->userable_id)->get();
    }


    public function delete($id)
    {
        return Customer::findOrFail($id)->delete();
    }

    public function update($request, $id)
    {
        return Customer::findOrFail($id)->update($request);
    }

    public function getOneCustomerById($id)
    {

        return Customer::findOrFail($id);
    }
}
