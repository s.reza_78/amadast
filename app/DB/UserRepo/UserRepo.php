<?php

namespace App\DB\UserRepo;

use App\DB\interfaces\UserRepoInterface;
use App\Models\User;

class UserRepo implements UserRepoInterface
{


    public function create($data)
    {
        return User::create($data);
    }
}
