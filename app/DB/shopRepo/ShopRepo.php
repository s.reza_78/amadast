<?php

namespace App\DB\shopRepo;

use App\DB\interfaces\ShopRepoInterface;
use App\Models\Shop;

class ShopRepo implements ShopRepoInterface
{

    public function create($data)
    {
        $data['seller_id'] = auth()->user()->userable->userable_id;
        $shop =  Shop::create($data);
        return $shop;
    }
    public function getAllSellerShops()
    {

        // Seller::find(auth()->user()->userable->userable_id)->shops;

        return Shop::where('seller_id' , auth()->user()->userable->userable_id)->get();
    }
}
