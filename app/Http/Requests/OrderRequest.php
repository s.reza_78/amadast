<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'shop_id'=>'numeric|exists:shops,id|required',
            'origin_address'=>'required',
            'destination_address'=>'required',
            'weight'=>'numeric|required',
            'recive_status'=>'required|in:recived,cancle,in progress',
            'payment_status'=>'required|in:payed,cancle,in progress',
            'origin_city'=>'required',
            'destination_city'=>'required',
            'sendingـmethod'=>'required|in:tipax'
        ];
    }
}
