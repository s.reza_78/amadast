<?php

namespace App\Http\Controllers\Api;

use App\DB\interfaces\CustomerRepoInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\CustomerRequest;
use App\Models\Customer;
use App\Models\Seller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{

    private CustomerRepoInterface $customerRepo;


    public function __construct(CustomerRepoInterface $customerRepo)
    {
        $this->customerRepo = $customerRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = $this->customerRepo->getAllSellerCustomers();

        return response(['customers' => $customers], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerRequest $request)
    {
        $data = $request->all();
        $createData = $this->customerRepo->create($data);
        if(isset($createData['user'])){
        return response(['user' => $createData['user'], 'customer' => $createData['customer']], 201);
        }
        return response(['message' => $createData['error'], "code" => 500], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customers = $this->customerRepo->getOneCustomerById($id);

        return response(['customer' => $customers], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $deleted = $this->customerRepo->update($request->all(),$id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
        $deleted = $this->customerRepo->delete($id);
        }
        catch(\Exception $e){
        return response()->json([ 'message' => $e->getMessage(),'status' => 500], 500);

        }
        if ($deleted) {
            return response()->json(['status' => 200, 'message' => 'customer deleted'], 200);
        }
        return response()->json(['status' => 404, 'message' => 'customer not found'], 404);
    }
}
