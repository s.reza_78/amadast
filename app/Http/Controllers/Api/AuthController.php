<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\UserRequest;
use App\Models\Seller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    public function register(UserRequest $request)
    {

        $userArray = $this->getUserArray($request);


        $seller = $this->getSellerArray($request);

        DB::beginTransaction();

        try {
            $userObj = User::create($userArray);
            $seller = Seller::create($seller);
            $seller->users()->save($userObj);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response(['message'=>$e->getMessage(),"code"=>500], 500);

        }

        $token = $userObj->createToken('LaravelAuthApp')->accessToken;

        return response(['user' => $userObj,'seller'=>$seller, 'token' => $token], 201);
    }

    public function login(LoginRequest $request)
    {
        $data = $request->all();

        if (!auth()->attempt($data)) {
            return response(['error_message' => 'Incorrect Details.
            Please try again']);
        }

        $token = auth()->user()->createToken('LaravelAuthApp')->accessToken;

        return response(['user' => auth()->user(), 'token' => $token]);
    }

    public function getUserArray($request)
    {

        $user['name'] = $request->name;
        $user['email'] = $request->email;
        $user['password'] = bcrypt($request->password);

        $user['password_confirmation'] = $request->password_confirmation;
        $user['phone'] = $request->phone;


        return $user;
    }

    public function getSellerArray($request)
    {
        $seller['phone'] = $request->phone;
        $seller['national_code'] = $request->national_code;
        $seller['address'] = $request->address;
        $seller['ostan'] = $request->ostan;
        $seller['city'] = $request->city;
        if (isset($request->shaba)) {
            $seller['shaba'] = $request->shaba;
        }

        if (isset($request->account_name)) {
            $seller['account_name'] = $request->account_name;
        }


        return $seller;
    }
}
