<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'shop_id'=>$this->shop_id,
            'support_code'=>$this->support_code,
            'description'=>$this->description,
            'origin_address'=>$this->origin_address,
            'destination_address'=>$this->destination_address,
            'weight'=>$this->weight,
            'recive_status'=>$this->recive_status,
            'invoice'=>$this->invoice,
            'payment_status'=>$this->payment_status,
            'origin_city'=>$this->origin_city,
            'destination_city'=>$this->destination_city,
            'sendingـmethod'=>$this->sendingـmethod
        ];
    }
}
