<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthenticationTest extends TestCase
{


    public function testMustEnterEmailAndPassword()
    {

        $this->json('POST', 'api/v1/login', ['Accept' => 'application/json'],)
            ->assertStatus(422)
            ->assertJsonValidationErrors(['email', 'password']);
    }

    public function testLoginWithoutError()
    {
        $userData = [
            "email" => "sreza@gmail.com",
            "password" => "123",
        ];
        $this->json('POST', 'api/v1/login', $userData, ['Accept' => 'application/json'],)
            ->assertStatus(200)
            ->assertJsonStructure([
                "user" => [
                    'id',
                    'name',
                    'email',
                    'created_at',
                    'updated_at',
                ],

                "token"

            ]);
    }
}
