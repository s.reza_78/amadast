<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('shop_id');
            $table->string('support_code')->unique()->index();
            $table->text('description')->nullable();
            $table->text('origin_address');
            $table->text('destination_address');
            $table->decimal('weight', 16, 2);
            $table->enum('recive_status',['done','cancle','in progress'])->default('in progress');
            $table->decimal('invoice',64,2);
            $table->enum('payment_status',['payed','in progress','faild'])->default('in progress');
            $table->string('origin_city');
            $table->string('destination_city');
            $table->string('sendingـmethod');



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
